import React from 'react';
import './board.css';
import {newGameState, makeMove, getGameStatus,
        makeCpuMove} from '../GameLogic/GameLogic.js';

function Square(props) {
    return (
        <button className="square" onClick={props.onClick}>
          {props.value}
        </button>
    );
}

export default class Board extends React.Component {
    constructor(props) {
        super(props);
        this.state = newGameState;
    }

    componentDidMount = () => this.setState(makeCpuMove(this.state));
    componentDidUpdate = () => this.setState(makeCpuMove(this.state));
    handleClick = (i) => this.setState(makeMove(this.state, i));
    resetGame = () => this.setState(newGameState);

    renderSquare = (i) => {
        return(
            <Square
              value={this.state.squares[i]}
              onClick={() => this.handleClick(i)}
              />
        );
    }

    renderRow = (cell1, cell2, cell3) => {
        return(
            <div className="board-row">
                {this.renderSquare(cell1)}
                {this.renderSquare(cell2)}
                {this.renderSquare(cell3)}
            </div>
        );
    };

    render() {
        let {message, gameOver} = getGameStatus(this.state);
        return (
            <div>
              {this.renderRow(0, 1, 2)}
              {this.renderRow(3, 4, 5)}
              {this.renderRow(6, 7, 8)}
              <div className="status">{message}</div>
              <button className="btn" hidden={!gameOver}
                      onClick={this.resetGame}>
                Restart game
              </button>
            </div>
        );
    }
}
