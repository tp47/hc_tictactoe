import cpuMove from './minimax.js';

export const getRandom = (lst) =>
    lst[Math.floor((Math.random()*lst.length))];

export const newGameState = {
    squares: Array(9).fill(null),
    xIsNext: getRandom([true, false]),
    moves: 0,
};

export const makeMove = (gameState, i) => {
    const {squares, xIsNext, moves}  = gameState;

    if (calculateWinner(squares) || squares[i])
        return ({});

    let squaresCopy = squares.slice();
    squaresCopy[i] = xIsNext? 'X' : 'O';
    return ({
        squares: squaresCopy,
        xIsNext: !xIsNext,
        moves: moves + 1,
    });
};

export const makeCpuMove = (gameState) =>
    (gameState.xIsNext || calculateWinner(gameState.squares)) ?
    null :
    cpuMove(gameState);

export const getGameStatus = (gameState) => {
    const {squares, xIsNext, moves}  = gameState;
    const winner = calculateWinner(squares);
    let status = {
        message: '',
        gameOver: (moves >= 9 || winner) ? true : false,
    };
    if(winner)
        status.message =  winner==='X'? "You win!": "Computer wins!";
    if (moves === 0)
        status.message =  "You start";
    if (moves === 1 && xIsNext)
        status.message = "Your turn";
    return status;
};

export const calculateWinner = (squares) => {
    const lines = [
        [0, 1, 2], [3, 4, 5], [6, 7, 8], [0, 3, 6],
        [1, 4, 7], [2, 5, 8], [0, 4, 8], [2, 4, 6],
    ];
    for (var line of lines) {
        const [a, b, c] = line;
        if (squares[a] &&
            squares[a] === squares[b] &&
            squares[a] === squares[c])
            return squares[a];
    }
    return null;
};
